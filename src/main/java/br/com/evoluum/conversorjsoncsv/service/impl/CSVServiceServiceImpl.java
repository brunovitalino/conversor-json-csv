package br.com.evoluum.conversorjsoncsv.service.impl;

import static br.com.evoluum.conversorjsoncsv.utils.FolderUtils.generateFoldersAndFile;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.opencsv.CSVWriter;

import br.com.evoluum.conversorjsoncsv.enums.FileTypeEnum;
import br.com.evoluum.conversorjsoncsv.enums.FolderTypeEnum;
import br.com.evoluum.conversorjsoncsv.service.CSVService;

@Service
public class CSVServiceServiceImpl implements CSVService {

	@Override
	public void saveCsv(String[] headers, List<String[]> itens, Optional<Long> userId, Optional<String> customName) throws Exception {
		System.out.println("Gerando CSV...");
		File csvFile;
		try {
			csvFile = generateFoldersAndFile(FolderTypeEnum.PUBLIC, userId, FileTypeEnum.CSV, customName);
		} catch (Exception e) {
			throw new RuntimeException(e.getCause());
		}
		
		try (FileWriter fw = new FileWriter(csvFile); CSVWriter cw = new CSVWriter(fw)) {

			List<String[]> data = new ArrayList<>();
			data.add(headers);
			itens.stream().forEach(data::add);

			cw.writeAll(data);

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("CSV gerado!");
	}
	
	@Override
	public void downloadCsv(HttpServletResponse response, Optional<Long> userId, Optional<String> customName)
			throws Exception {
		System.out.println("Baixando CSV...");
		File csvFile;
		try {
			csvFile = generateFoldersAndFile(FolderTypeEnum.PUBLIC, userId, FileTypeEnum.CSV, customName);
		} catch (Exception e) {
			throw new RuntimeException(e.getCause());
		}

		if (csvFile.exists()) {

			// get the mimetype
			String mimeType = URLConnection.guessContentTypeFromName(csvFile.getName());
			if (mimeType == null) {
				// unknown mimetype so set the mimetype to application/octet-stream
				mimeType = "application/octet-stream";
			}

			response.setContentType(mimeType);

			/**
			 * In a regular HTTP response, the Content-Disposition response header is a
			 * header indicating if the content is expected to be displayed inline in the
			 * browser, that is, as a Web page or as part of a Web page, or as an
			 * attachment, that is downloaded and saved locally.
			 * 
			 */

			/**
			 * Here we have mentioned it to show inline
			 */
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + csvFile.getName() + "\""));

			// Here we have mentioned it to show as attachment
			// response.setHeader("Content-Disposition", String.format("attachment;
			// filename=\"" + file.getName() + "\""));

			response.setContentLength((int) csvFile.length());

			InputStream inputStream = new BufferedInputStream(new FileInputStream(csvFile));

			FileCopyUtils.copy(inputStream, response.getOutputStream());

		}
		System.out.println("Download concluído!");
	}

}
