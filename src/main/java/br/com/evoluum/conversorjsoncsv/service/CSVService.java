package br.com.evoluum.conversorjsoncsv.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

public interface CSVService {

	/**
	 * Saves a CSV file using headers and data as content. Uses userId and
	 * customName to set destiny folders.
	 * @param headers file headers.
	 * @param data file content.
	 * @param used when configuring the user's folder path.
	 * @param customName used when configuring the user's file name.
	 * @throws Exception
	 */
	public void saveCsv(String[] headers, List<String[]> data, Optional<Long> userId, Optional<String> customName) throws Exception;
	
	/**
	 * Download the CSV file.
	 * @param response used to write on the OutputStream.
	 * @param userId if necessary, used to find the user's folder path.
	 * @param customName if necessary, used to find the user's file name. 
	 * @throws Exception
	 */
	public void downloadCsv(HttpServletResponse response, Optional<Long> userId, Optional<String> customName) throws Exception;

}
