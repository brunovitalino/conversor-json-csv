package br.com.evoluum.conversorjsoncsv.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import br.com.evoluum.conversorjsoncsv.entity.Localidade;

public interface LocalidadeService {

	/**
	 * Loads all Localidade throught an external API.
	 * @return a list of Localidade.
	 * @throws Exception
	 */
	public List<Localidade> loadAll() throws Exception;
	
	// It needs bugfix. One Localidade to a list of Localidade for filter nomeCidade
	/**
	 * Loads all Localidade by nomeCidade throught an external API.
	 * @param nomeCidade
	 * @return a list of Localidade.
	 * @throws Exception
	 */
	public Optional<Localidade> loadAllByNomeCidade(String nomeCidade) throws Exception;
	
	// It needs bugfix. One idCidade to a list of idCidade for filter nomeCidade
	/**
	 * Loads all idCidade by nomeCidade throught an external API.
	 * @param nomeCidade
	 * @return a list of idCidade.
	 * @throws Exception
	 */
	public Optional<Long> getIdCidadeByNomeCidade(String nomeCidade) throws Exception;

	/**
	 * Generates a CSV file with all Localidade throught an external API.
	 * @throws Exception
	 */
	public void generateCsv() throws Exception;

	/**
	 * Download a CSV file with all Localidade.
	 * @param response
	 * @throws Exception
	 */
	public void downloadCsv(HttpServletResponse response) throws Exception;

}
