package br.com.evoluum.conversorjsoncsv.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.com.evoluum.conversorjsoncsv.entity.Localidade;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Estado;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Municipio;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.service.IBGEEstadoService;
import br.com.evoluum.conversorjsoncsv.service.CSVService;
import br.com.evoluum.conversorjsoncsv.service.LocalidadeService;

@Service
public class LocalidadeServiceImpl implements LocalidadeService {
	
	@Autowired
	IBGEEstadoService iBGEEstadoService;
	
	@Autowired
	CSVService cSVService;

	@Override
	@Cacheable("localidadeGenerateCsv")
	public List<Localidade> loadAll() throws Exception {
//		LocalTime start = LocalTime.now();
	
		List<Localidade> localidades = new ArrayList<>();
		List<Estado> estados = iBGEEstadoService.loadAllBlocking();

		for (Estado e : estados) {
			List<Municipio> municipiosByEstado = iBGEEstadoService.loadAllMunicipioByEstadoIdBlocking(e.getId());
			municipiosByEstado.forEach(m -> localidades.add(
					new Localidade(e.getId(),
							e.getSigla(),
							e.getRegiao() == null ? "" : e.getRegiao().getNome(),
							m.getNome(),
							m.getMicrorregiao() == null ? ""
									: m.getMicrorregiao().getMesorregiao() == null ? ""
											: m.getMicrorregiao().getMesorregiao().getNome(),
							m.getId())));
		}
		;
//		for (int i = 0; i < estados.size(); i++) {
//			Estado estado = estados.get(i);
//			List<Municipio> municipiosByEstado = iBGEEstadoService
//					.loadAllMunicipioByEstadoIdBlocking(estado.getId());
//			municipiosByEstado
//					.forEach(
//							m -> localidades
//									.add(new Localidade(
//											estado.getId(),
//											estado.getSigla(),
//											estado.getRegiao() == null ? "" : estado.getRegiao().getNome(),
//											m.getNome(),
//											m.getMicrorregiao() == null ? ""
//													: m.getMicrorregiao().getMesorregiao() == null ? ""
//															: m.getMicrorregiao().getMesorregiao().getNome(),
//											m.getId())));
//		}
		
		// Otimizacao sera realizada depois
//		List<Mono<List<Municipio>>> municipiosReact = estados.stream()
//				.map(e -> {
//					try {
//						Thread.sleep(50);
//						return iBGEEstadoService.loadAllMunicipioByEstadoIdReactive(e.getId());
//					} catch (Exception e1) {
//						throw new RuntimeException();
//					}
//				}).collect(Collectors.toList());
//		List<List<Municipio>> listOfMunicipiosListByEstado = municipiosReact.stream().map(ml -> ml.block())
//				.collect(Collectors.toList());
//		List<Municipio> municipios = new ArrayList<>();
//		listOfMunicipiosListByEstado.stream().forEach(municipios::addAll);
//		municipios.stream().forEach(m -> localidades.add(new Localidade(null, null, null, null, null)));
		
//		LocalTime end = LocalTime.now();
//		System.err.println("Localidade::loadAll elapsed time: " + Duration.between(start, end).toMillis());
		return localidades;
	}

	@Override
	public Optional<Localidade> loadAllByNomeCidade(String nomeCidade) throws Exception {
		List<Localidade> localidades = loadAll();
		return localidades.stream()
				.filter(l -> nomeCidade.equals(l.getNomeCidade()))
				.findFirst();
	}

	@Override
	@Cacheable("localidadeGenerateCsv")
	public Optional<Long> getIdCidadeByNomeCidade(String nomeCidade) throws Exception {
		Optional<Localidade> localidade = loadAllByNomeCidade(nomeCidade);
		return localidade.isPresent() ? Optional.ofNullable(localidade.get().getIdCidade()) : Optional.empty();
	}

	@Override
	@CacheEvict(value = "localidadeGenerateCsv", allEntries = true)
	public void generateCsv() throws Exception {
		List<Localidade> localidades = loadAll();
		Optional<String> customName = Optional.of("localidades");
		String[] headers = {"idEstado", "siglaEstado", "nomeRegiao", "nomeCidade", "nomeMesorregiao", "nomeFormatado"};
		List<String[]> itens = new ArrayList<>();
		localidades.stream()
				.forEach(l -> {
					String[] item = { l.getIdEstado().toString(), l.getSiglaEstado(), l.getRegiaoNome(), l.getNomeCidade(),
							l.getNomeMesorregiao(), l.getNomeFormatado() };
					itens.add(item);
				});
		cSVService.saveCsv(headers, itens, Optional.empty(), customName);
	}
	
	@Override
	public void downloadCsv(HttpServletResponse response) throws Exception {
		Optional<String> customName = Optional.of("localidades");
		cSVService.downloadCsv(response, Optional.empty(), customName);
	}

}
