package br.com.evoluum.conversorjsoncsv.utils.custom;

import java.io.File;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PrivateTempCustomFoldersPath implements CustomFoldersPath {

	@Override
	public String getPath() {
		return "private" + File.separator + "temp";
	}

}
