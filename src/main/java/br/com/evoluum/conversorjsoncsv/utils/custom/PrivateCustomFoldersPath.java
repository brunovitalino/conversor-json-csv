package br.com.evoluum.conversorjsoncsv.utils.custom;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PrivateCustomFoldersPath implements CustomFoldersPath {

	@Override
	public String getPath() {
		return "private";
	}

}
