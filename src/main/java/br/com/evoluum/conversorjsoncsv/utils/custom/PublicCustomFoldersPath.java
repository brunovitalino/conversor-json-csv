package br.com.evoluum.conversorjsoncsv.utils.custom;

import java.io.File;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PublicCustomFoldersPath implements CustomFoldersPath {

	@Override
	public String getPath() {
		return "public" + File.separator + "temp";
	}

}
