package br.com.evoluum.conversorjsoncsv.utils;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import br.com.evoluum.conversorjsoncsv.enums.FileTypeEnum;
import br.com.evoluum.conversorjsoncsv.enums.FolderTypeEnum;

public class FolderUtils {

	// FOLDER PATTERN: public / temp / fileName.extension
	// FOLDER PATTERN: private / userId / fileType / datas / fileNameByTime.extension
	public static File generateFoldersAndFile(FolderTypeEnum folderType, Optional<Long> userId,
			FileTypeEnum fileType, Optional<String> customFileName) throws Exception {

		String dateTimeToString = getDateTimeToString();
		String randomFileName = dateTimeToString.substring(dateTimeToString.lastIndexOf(File.separator) + 1);
		
		String foldersPath = getFoldersPath(folderType, dateTimeToString, userId);
		
		generateFoldersIfNotExists(foldersPath);

		return new File(foldersPath + File.separator
				+ (customFileName.isPresent() ? customFileName.get() : randomFileName) + fileType.getExtension());
	}

	private static String getDateTimeToString() {
		LocalDateTime now = LocalDateTime.now();
		return now.format(DateTimeFormatter
				.ofPattern("yyyy" + File.separator + "MM" + File.separator + "dd" + File.separator + "HHmmssSSS"));
	}
	
	private static String getFoldersPath(FolderTypeEnum folderType, String dateTimeToString, Optional<Long> userId) throws Exception {
		if (FolderTypeEnum.PUBLIC.equals(folderType)) {
			return FolderTypeEnum.PUBLIC.getValue().getPath();
		}
		if (FolderTypeEnum.PRIVATE.equals(folderType) && userId.isPresent()) {
			String foldersByDate = dateTimeToString.substring(0, dateTimeToString.lastIndexOf(File.separator));
			return folderType.getValue().getPath() + File.separator + userId.toString() + File.separator + foldersByDate;
		}
		throw new Exception("Tipo de pasta desconhecida!");
	}

	private static void generateFoldersIfNotExists(String path) {
		File file1 = new File(path);
		if (!file1.exists()) {
			file1.mkdirs();
		}
	}

}
