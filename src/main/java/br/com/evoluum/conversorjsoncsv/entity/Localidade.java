package br.com.evoluum.conversorjsoncsv.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Localidade {
	private Long idEstado;
	private String siglaEstado;
	private String regiaoNome;
	private String nomeCidade;
	private String nomeMesorregiao;
	private Long idCidade;

	public Localidade(Long idEstado, String siglaEstado, String regiaoNome, String nomeCidade, String nomeMesorregiao) {
		this.idEstado = idEstado;
		this.siglaEstado = siglaEstado;
		this.regiaoNome = regiaoNome;
		this.nomeCidade = nomeCidade;
		this.nomeMesorregiao = nomeMesorregiao;
	}

	public String getNomeFormatado() {
		return nomeCidade + "/" + siglaEstado;
	}

}
