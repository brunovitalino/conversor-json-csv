package br.com.evoluum.conversorjsoncsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.evoluum.conversorjsoncsv.config.webclient.CustomExchangeFilter;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableCaching
@EnableSwagger2
public class ConversorJsonCsvApplication {
	
	@Bean
	public WebClient webClientIBGELocalidades(WebClient.Builder builder) {
		String IBGEUrl = "https://servicodados.ibge.gov.br/api/v1/localidades";
		return builder
				.baseUrl(IBGEUrl)
				.filter(new CustomExchangeFilter())
				.exchangeStrategies(ExchangeStrategies.builder()
						.codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
				.build();
	}

	public static void main(String[] args) {
		SpringApplication.run(ConversorJsonCsvApplication.class, args);
	}

}
