package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity;

import lombok.Data;

@Data
public class Microrregiao {
	private Long id;
	private String nome;
	private Mesorregiao mesorregiao;
}
