package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity;

import lombok.Data;

@Data
public class RegiaoIntermediaria {
	private Long id;
	private String nome;
	private UF uF;
}
