package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity;

import lombok.Data;

@Data
public class RegiaoImediata {
	private Long id;
	private String nome;
	private RegiaoIntermediaria regiaoIntermediaria;
}
