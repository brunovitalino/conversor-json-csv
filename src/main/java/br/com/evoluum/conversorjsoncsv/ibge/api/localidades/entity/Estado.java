package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity;

import lombok.Data;

@Data
public class Estado {
	private Long id;
	private String sigla;
	private String nome;
	private Regiao regiao;
}
