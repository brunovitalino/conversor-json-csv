package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Estado;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Municipio;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.service.IBGEEstadoService;
import reactor.core.publisher.Mono;

@Service
public class IBGEEstadoServiceImpl implements IBGEEstadoService {
	private final String SERVICE_PATH = "/estados";
	
	@Autowired
	WebClient webClientIBGELocalidades;
	
	@Override
	public	Mono<List<Estado>> loadAllReactive() throws Exception {
		return webClientIBGELocalidades
				.get()
				.uri(SERVICE_PATH)
				.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
				.retrieve()
				.bodyToMono(new ParameterizedTypeReference<List<Estado>>() {});
	}
	
	@Override
	public	List<Estado> loadAllBlocking() throws Exception {
		return loadAllReactive().block();
	}

	@Override
	public Mono<List<Municipio>> loadAllMunicipioByEstadoIdReactive(Long estadoId) throws Exception {
		String endPoint = "/{estadoId}/municipios";
		Map<String, String> params = new HashMap<>();
		params.put("estadoId", estadoId.toString());
		String url = SERVICE_PATH + endPoint;
		return webClientIBGELocalidades
				.get()
				.uri(url, params)
				.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
				.retrieve()
				.bodyToMono(new ParameterizedTypeReference<List<Municipio>>() {})
				.onErrorMap(Exception::new);
	}
	
	@Override
	public List<Municipio> loadAllMunicipioByEstadoIdBlocking(Long estadoId) throws Exception {
		return loadAllMunicipioByEstadoIdReactive(estadoId).block();
	}

}
