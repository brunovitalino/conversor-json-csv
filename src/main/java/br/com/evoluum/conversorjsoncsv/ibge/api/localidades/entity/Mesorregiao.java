package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Mesorregiao {
	private Long id;
	private String nome;
	@JsonProperty("UF")
	private UF uF;
}
