package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.service;

import java.util.List;

import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Estado;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Municipio;
import reactor.core.publisher.Mono;

public interface IBGEEstadoService {
	
	/**
	 * Accesses the IBGE's external API to get all Estado in reactive mode.
	 * @return a list of Estado.
	 * @throws Exception
	 */
	public	Mono<List<Estado>> loadAllReactive() throws Exception;
	
	/**
	 * Accesses the IBGE's external API to get all Estado
	 * @return a list of Estado.
	 * @throws Exception
	 */
	public	List<Estado> loadAllBlocking() throws Exception;
	
	/**
	 * Accesses the IBGE's external API to get all Municipio in reactive mode.
	 * @param estadoId the Estado id.
	 * @return a list of Municipio.
	 * @throws Exception
	 */
	public	Mono<List<Municipio>> loadAllMunicipioByEstadoIdReactive(Long estadoId) throws Exception;
	
	/**
	 * Accesses the IBGE's external API to get all Municipio.
	 * @param estadoId
	 * @return a list of Municipio.
	 * @throws Exception
	 */
	public	List<Municipio> loadAllMunicipioByEstadoIdBlocking(Long estadoId) throws Exception;

}
