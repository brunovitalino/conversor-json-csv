package br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity;

import lombok.Data;

@Data
public class Municipio {
	private Long id;
	private String nome;
	private Microrregiao microrregiao;
	private RegiaoImediata regiaoImediata;
}
