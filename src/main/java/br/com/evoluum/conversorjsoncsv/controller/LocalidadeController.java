package br.com.evoluum.conversorjsoncsv.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.evoluum.conversorjsoncsv.entity.Localidade;
import br.com.evoluum.conversorjsoncsv.service.LocalidadeService;

@RestController
@RequestMapping("/localidades")
public class LocalidadeController {
	
	@Autowired
	LocalidadeService localidadeService;
	
	@GetMapping
	public ResponseEntity<List<Localidade>> loadAll() {
		try {
			List<Localidade> localidades = localidadeService.loadAll();
			return ResponseEntity.ok(localidades);
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping("/nomeCidade/{nomeCidade}")
	public ResponseEntity<Localidade> loadAllByNomeCidade(@PathVariable String nomeCidade) {
		try {
			Optional<Localidade> localidade = localidadeService.loadAllByNomeCidade(nomeCidade);
			return localidade.isPresent() ? ResponseEntity.ok(localidade.get()) : ResponseEntity.notFound().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping("/nomeCidade/{nomeCidade}/idCidade")
	public ResponseEntity<Long> getIdCidadeByNomeCidade(@PathVariable String nomeCidade) {
		try {
			Optional<Long> idCidade = localidadeService.getIdCidadeByNomeCidade(nomeCidade);
			return idCidade.isPresent() ? ResponseEntity.ok(idCidade.get()) : ResponseEntity.notFound().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@GetMapping("/gerar/csv")
	public ResponseEntity<String> generateCsv() {
		try {
			localidadeService.generateCsv();
			return ResponseEntity.ok("CSV gerado!");
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@GetMapping("/baixar/csv")
	public ResponseEntity<?> downloadCsv(HttpServletRequest request, HttpServletResponse response) {
		try {
			localidadeService.downloadCsv(response);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

}
