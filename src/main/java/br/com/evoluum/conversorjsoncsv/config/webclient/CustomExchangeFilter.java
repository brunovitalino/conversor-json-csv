package br.com.evoluum.conversorjsoncsv.config.webclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;

import reactor.core.publisher.Mono;

public class CustomExchangeFilter implements ExchangeFilterFunction {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomExchangeFilter.class);

	@Override
	public Mono<ClientResponse> filter(ClientRequest request, ExchangeFunction next) {
		LOGGER.info("Executing external API...");
		LOGGER.info("Headers: {}", request.headers());
		LOGGER.info("Request Method: {}", request.method());
		LOGGER.info("Request URI: {}", request.url());
		return next.exchange(request);
	}

}
