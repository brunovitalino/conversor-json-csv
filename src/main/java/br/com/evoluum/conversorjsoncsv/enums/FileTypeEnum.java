package br.com.evoluum.conversorjsoncsv.enums;

public enum FileTypeEnum {
	CSV("CSV", ".csv");
	
	String value;
	String extension;
	
	FileTypeEnum(String value, String extension) {
		this.value = value;
		this.extension = extension;
	}

	public String getValue() {
		return value;
	}

	public String getExtension() {
		return extension;
	}
}
