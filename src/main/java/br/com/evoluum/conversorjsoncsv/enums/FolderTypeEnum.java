package br.com.evoluum.conversorjsoncsv.enums;

import br.com.evoluum.conversorjsoncsv.utils.custom.CustomFoldersPath;
import br.com.evoluum.conversorjsoncsv.utils.custom.PrivateCustomFoldersPath;
import br.com.evoluum.conversorjsoncsv.utils.custom.PrivateTempCustomFoldersPath;
import br.com.evoluum.conversorjsoncsv.utils.custom.PublicCustomFoldersPath;

public enum FolderTypeEnum {
	PUBLIC(new PublicCustomFoldersPath()),
	PRIVATE(new PrivateCustomFoldersPath()),
	PRIVATE_TEMP(new PrivateTempCustomFoldersPath());

	CustomFoldersPath value;
	
	FolderTypeEnum(CustomFoldersPath value) {
		this.value = value;
	}

	public CustomFoldersPath getValue() {
		return value;
	}
}
