package br.com.evoluum.conversorjsoncsv.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.evoluum.conversorjsoncsv.entity.Localidade;

@SpringBootTest
public class LocalidadeServiceTest {
	
	@Autowired
	LocalidadeService localidadeService;

	@Test
	void testSuccessLoadAll() {
		try {
			List<Localidade> localidades = localidadeService.loadAll();
			assertTrue(localidades.size() > 0);
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

	@Test
	void testSuccessLoadAllByNomeCidade() {
		String nomeCidade = "Fortaleza";
		try {
			Optional<Localidade> localidade = localidadeService.loadAllByNomeCidade(nomeCidade);
			assertTrue(localidade.isPresent());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

	@Test
	void testSuccessGetIdCidadeByNomeCidade() {
		String nomeCidade = "Fortaleza";
		try {
			Optional<Long> idCidade = localidadeService.getIdCidadeByNomeCidade(nomeCidade);
			assertTrue(idCidade.isPresent());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

	@Test
	void testSuccessObterCsv() {
		try {
			localidadeService.generateCsv();
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

}
