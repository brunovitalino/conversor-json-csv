package br.com.evoluum.conversorjsoncsv.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CSVServiceTest {
	
	@Autowired
	CSVService localidadeService;

	@Test
	void testSuccessGerarCsv() {
		Optional<Long> userId = Optional.of(1L);
		Optional<String> customName = Optional.ofNullable("csvGerado");
		try {
			String[] headers = { "nome", "email" };
			List<String[]> itens = new ArrayList<>();
			String[] item1 = { "Bruno Vitalino", "bv@bv.com" };
			String[] item2 = { "Diego Alcântara", "da@da.com" };
			itens.add(item1);
			itens.add(item2);
			localidadeService.saveCsv(headers, itens, userId, customName);
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

}
