package br.com.evoluum.conversorjsoncsv.ibge.localidades.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Estado;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.entity.Municipio;
import br.com.evoluum.conversorjsoncsv.ibge.api.localidades.service.IBGEEstadoService;

@SpringBootTest
public class IBGEEstadoServiceTest {
	
	@Autowired
	IBGEEstadoService estadoService;

	@Test
	void testSuccessLoadAllReactive() {
		try {
			List<Estado> estados = estadoService.loadAllReactive().block();
			assertFalse(estados == null);
			assertEquals(27, estados.size());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

	@Test
	void testSuccessLoadAllBlocking() {
		try {
			List<Estado> estados = estadoService.loadAllBlocking();
			assertFalse(estados == null);
			assertEquals(27, estados.size());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

	@Test
	void testSuccessLoadAllMunicipioByEstadoIdReactive() {
		Long estadoId = 23L; // Ceara
		try {
			List<Municipio> municipios = estadoService.loadAllMunicipioByEstadoIdReactive(estadoId).block();
			assertFalse(municipios == null);
			assertEquals(184, municipios.size());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

	@Test
	void testSuccessLoadAllMunicipioByEstadoIdBlocking() {
		Long estadoId = 23L;
		try {
			List<Municipio> municipios = estadoService.loadAllMunicipioByEstadoIdBlocking(estadoId);
			assertFalse(municipios == null);
			assertEquals(184, municipios.size());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

}
