# API Localidades  

API que obtem informações externas de Localidade através da API do IBGE e converter para um conjunto único de informações no formato JSON por endpoints ou então através do download de um arquivo CSV.  

## Serviço  

- Localidades;    

## Sites

[Documentação](http://localhost:8080/swagger-ui.html)  
[Aplicação](http://localhost:8080)  

## Instruções

#### Rodando localmente  

* Empacotador: `Maven 3.6.3`  
* JVM: `Java 11`  

Clone o repositório do projeto para uma pasta local do computador, abra o terminal ou prompt de comando, e realize os seguintes passos:

1. Entre na pasta raiz do projeto clonado e execute o comando:  
``mvn clean package``  

2. Em seguida, suba o projeto com o comando:  
``java -jar target/evoluum.jar``  

3. Faça chamada aos recursos que deseja:  
`GET` [/localidades](http://localhost:8080/localidades) (exemplo)  
